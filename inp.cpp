#include <stdio.h>
#include <stdlib.h>

int main(){
    int ans = 0;
    int i;
    char ch;

    while ( scanf("%d", &i)==1 ){
        ans += i;

        while ((ch=getchar()) == ' '){}
        if (ch==10){
            break;
        }
        ungetc(ch, stdin);
    }
    printf("ans = %d\n", ans);
    return 0;
}
