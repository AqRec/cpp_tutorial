#include <iostream>
#include <vector>

int main(){
    int n = 10;
    std::vector<int> nums;
    nums.push_back(1);
    nums.push_back(1);
    int a;
    for (int i=2; i<n; i++){
        a = *(nums.end()-1) + *(nums.end()-2);
        // a = nums[i-1] + nums[i-2];   
        nums.push_back(a);
    }
    for (int i=0;i<n;i++){
        std::cout << nums[i] << "\n";
    }

    return 0;
}
