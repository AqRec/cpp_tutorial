#include <iostream>
#include <vector>
#define LENGTH 10


int main(){
	std::vector<int> nums(2, 1);
	int a;
	for (int i=2; i<LENGTH; i++){
		a = *(nums.end()-1) + *(nums.end()-2);
		nums.push_back(a);
	}

	std::vector<int>::iterator iter = nums.begin();
	while (iter != nums.end()){
		std::cout << *(iter) << std::endl;
		iter++;
	}
	return 0;
}
