// Fahrenheit = Celsius * 9/5 + 32

# include <iostream>

int main(){
    char from; float num; float ans;
    std::cout << "from? ";
    std::cin >> from;
    std::cout << "num? ";
    std::cin >> num;

    if (from=='f' || from=='F'){
        ans = (num - 32) * 5 / 9;
        std::cout << num << " Farenheit = " << ans << " Celsius" << std::endl;
    } else if (from=='c' || from=='C'){
        ans = num * 9 / 5 + 32;
        std::cout << num << " Celsius = " << ans << " Fahrenheit" << std::endl;
    }
    return 0;
}