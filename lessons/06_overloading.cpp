/* define a function `multiply`
 * if inputs is one interger, return sqrt
 * if inputs are 2 intergers, return their multiplication
 */

# include <iostream>

int multiply(int a, int b);
int multiply(int c);

int main(){
	int a, b, c, ans1, ans2;
	std::cout << "input one number" << std::endl;
	std::cin >> c;
	ans1 = multiply(c);
	std::cout << "ans1 = " << ans1 << std::endl;

	std::cout << "input 2 numbers" << std::endl;
	std::cin >> a >> b;
	ans2 = multiply(a, b);
	std::cout << "ans2 = " << ans2 << std::endl;

	//std::cout << "1 num:" << c << " |2 nums:" << a << ", " << b << std::endl;

}

int multiply(int c){
	return c*c;
}

int multiply(int a, int b){
	return a*b;
}
