#include <iostream>
#define LENGTH 10

int main(){
	int arr[LENGTH];
	int sum = 0;
	for (int i=0; i<LENGTH; i++){
		std::cout << "input the " << i+1 << "-th number" << std::endl;
		while (!(std::cin >> arr[i])){
			std::cout << "invalid value, try again" << std::endl;
			std::cin.clear();
			std::cin.ignore(100, '\n');
		}
		sum += arr[i];
	}
	std::cout << "sum = " << sum << std::endl << "avg = " << (float)sum/LENGTH << std::endl;
}
