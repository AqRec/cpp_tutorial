#include <iostream>

int main(){
    std::cout << "inputs:\n";
    int i;
    int ans = 0;
    while (std::cin >> i){
        /* 如果读到 EOF 或 非法值 std::cin也会返回0
         * 也就是说EOF也可以终止循环
         * Linux下的 EOF 是 按两次Ctrl+D
         */
        ans += i;
        while (std::cin.peek() == ' '){
            std::cin.get();
        }
        if (std::cin.peek() == '\n'){
            break;
        }
    }
    std::cout << "ans = " << ans << "\n" << std::endl;
    return 0;
}