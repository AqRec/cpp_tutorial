#include <iostream>
#include <string>

int main(){
    // std::string s;
    char s[30];
    std::cin.ignore(5);
    std::cin.getline(s,3);
    std::cout << (s[2]=='\0') << "\n";
    /* 注意 == 的优先级比 << 高
     * 以及你猜的没错。s[2] 确实是'\0'
     * cin.getline(一个字符串， 长度（含'\0'）)
     */
    printf("%s\n", s);
    return 0;
}