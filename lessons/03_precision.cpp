#include <iostream>
#include <math.h>

using namespace std;
int main(){
    double res = sqrt(3);

    for (int i = 0;i<10;i++){
        cout.precision(i);
        cout << "i=" << i << "\tres = " << res << endl;
    }
    return 0;
}
/*
res = sqrt(3):
i=0     res = 2
i=1     res = 2
i=2     res = 1.7
i=3     res = 1.73
i=4     res = 1.732
i=5     res = 1.7321
i=6     res = 1.73205
i=7     res = 1.732051
i=8     res = 1.7320508
i=9     res = 1.73205081

res = sqrt(0.5):
i=0     res = 0.7
i=1     res = 0.7
i=2     res = 0.71
i=3     res = 0.707
i=4     res = 0.7071
i=5     res = 0.70711
i=6     res = 0.707107
i=7     res = 0.7071068
i=8     res = 0.70710678
i=9     res = 0.707106781

res = sqrt(0.05)
i=0     res = 0.07
i=1     res = 0.07
i=2     res = 0.071
i=3     res = 0.0707
i=4     res = 0.07071
i=5     res = 0.070711
i=6     res = 0.0707107
i=7     res = 0.07071068
i=8     res = 0.070710678
i=9     res = 0.0707106781
 */