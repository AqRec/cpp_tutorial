#include <iostream>

using namespace std;
int main(){
    cout << "inputs\n";
    const int SIZE = 50;
    char s[SIZE];

    cin.ignore(2);
    s[5] = 'x';
    cin.read(s, 5);
    /*
    inputs
    123456789abcdefg
    gcount = 5
    s = 34567x
    所以 cin.read(s, 5) 搭配 cout.write(s, 5)
     */

    // cin.getline(s, 6);
    /*
    inputs
    123456789abcdefg
    gcount = 5
    s = 34567
     */

    cout << "gcount = " << cin.gcount() << "\ns = " << s << "\n";
    /*
    std::streamsize std::istream::gcount() const
    @brief Character counting
    @return The number of characters extracted by the previous
    unformatted input function dispatched for this stream.
     */
    return 0;
}