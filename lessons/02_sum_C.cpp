#include <stdio.h>

#include <iostream>
#include <typeinfo>

int sumArray(int arr[], int size);

using namespace std;
int main(){
    int arr[] = {1,2,3,4,5,6,7,8,9};
    /* the arr here is an array.
     * if we put a break here, we can see from debugger that arr has an attribute named "size"
     * we cannot get it with arr.size, its value also looks meaningless...
     * but we can know "how many bytes is arr occupying" with sizeof(arr),
     *     and get length with "sizeof(arr)/sizeof(arr[0])",
     * if we print &arr here (which is its pointer), 
     *     it will match with the pointer passing to function sumArray thereafter.
     */
    std::cout << "in main: " << typeid(arr).name() << "\n";
    std::cout << sizeof(arr) << "\n";
    std::cout << sizeof(arr[0]) << "\n";

    int size = sizeof(arr)/sizeof(arr[0]);
    printf("sum is %d\n", sumArray(arr, size));
}

int sumArray(int *arr, int size){
    /* the arr here is a pointer.
     * that is why sizeof(arr) cannot get the size of this array,
     *     but size of a pointer, referring to the address of array.
     * since arr is a point, arr[1] <=> *(arr + 1)
     * that is why we need to get the size of array before it is passed into another function.
     * because what is actually passed in, is not the array itself.
     */
    std::cout << "in function: " << typeid(arr).name() << "\n";
    std::cout << sizeof(arr) << "\n";
    std::cout << sizeof(arr[0]) << "\n";

    int ans = 0;
    for (int i=0;i<size;i++){
        ans += *(arr+i);
    }
    return ans;
}

